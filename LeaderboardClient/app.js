var express = require('express')
var app = express()

app.use(express.static('public'))

var fs = require('fs') // this engine requires the fs module
app.engine('html', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(err)
    // this is an extremely simple template engine
    var rendered = content.toString()
    return callback(null, rendered)
  })
})
app.set('views', './views') // specify the views directory
app.set('view engine', 'html') // register the template engine

app.get('/', function (req, res) {
  res.render('index')
})

app.get('/admin', function(req, res){
  res.render('admin')
})

app.listen(8000, function () {
  console.log('Test client listening on port 8000!')
})