var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

router.route('/login')
    .post(function(req, res){
        if(req.body.username == 'admin' && req.body.password == 'admin')
        {
            var token = jwt.sign({ id: 'admin' }, 'secret', {
                expiresIn: 86400 // expires in 24 hours
              });
              res.json({token: token});
        }
        else
        {
            res.status(500).send("Access denied.")
        }
    });
    
module.exports = router;