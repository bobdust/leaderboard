var express = require('express');
var router = express.Router();

var WebSocketServer = require('ws').Server,
  wss = new WebSocketServer({port: 40510});
wss.on('connection', function (ws) {
    console.log('connected');
});
wss.broadcast = function (data) {
    this.clients.forEach(function(client){
        client.send(JSON.stringify(data));
    });
};

var User = require('../models/user');
var saveUser = function(req, res){
    try{
        var name = req.body.name;
        var score = req.body.score;
        var user = User.find(name);
        if(user)
        {
            user.score = score;
        }
        else
        {
            user = new User();      
            user.name = name;
            user.score = score;
        }
        user.save();
        var projection = {id: user.id, name: user.name, score: user.score};
        wss.broadcast(projection);
        res.json(projection);
    }
    catch(err){
        res.status(500).send(err);
    }
};

router.route('/users')
    .post(function(req, res) {
        saveUser(req, res);
    })
    .put(function(req, res){
        saveUser(req, res);
    });
  
module.exports = router;
