var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var User = require('../models/user');

var verifyToken = function(req, res, success){
    var token = req.headers['x-access-token'];
    if (!token) 
    {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, 'secret', function(err, decoded) {
        if (err) 
        {
            res.status(500).send({ 
                auth: false, 
                message: 'Failed to authenticate token.' 
            });
        }
        else
        {
            success();
        }
    });
};

router.route('/users')
    .get(function(req, res){
        verifyToken(req, res, function(){
            res.json(User.list());
        });
    });
router.route('/users/:id')
    .delete(function(req, res){
        verifyToken(req, res, function(){
            var id = req.params.id
            User.delete(id);
            res.json({id: id});
        });
    });
router.route('/users/:from/:to')
    .get(function(req, res){
        verifyToken(req, res, function(){
            var from = Date.parse(req.params.from);
            var to = Date.parse(req.params.to);
            var filtered = User.countUpdates(from, to);
            res.json(filtered);
        });
    });

module.exports = router;