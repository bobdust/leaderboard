var store = [];
var log = [];
var uuid = require('node-uuid');
var User = function(){
};
User.prototype = {
    save: function(){
        if(this.id)
        {
            this.revision++;
            this.modified = new Date();
            log.push({id: this.id, timestamp: this.modified});
        }
        else
        {
            this.id = uuid.v1();
            this.revision = 0;
            this.created = this.modified = new Date();
            store.push(this);
        }
    }
};
User.find = function(name){
    return store.find(function(u){
        return u.name.toLowerCase() == name.toLowerCase();
    });
};
User.findById = function(id){
    return store.find(function(u){
        return u.id == id;
    });
};
User.list = function(){
    return store;
};
User.delete = function(id){
    var index = store.findIndex(function(u){
        return u.id == id;
    });
    if(index >= 0)
    {
        store.splice(index, 1);
    }
};
User.countUpdates = function(from, to){
    var filtered = log.filter(function(e, i, a){
        return e.timestamp >= from && e.timestamp <= to;
    }).filter(function(e, i, a){
        return a.indexOf(e) == i;
    });
    return filtered;
};
module.exports = User;