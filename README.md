**Prerequisites**

1. [Download and install NodeJS](https://nodejs.org/en/download/).

2. Web browser that supports web socket
*(should be fine with most of modern browsers nowadays)*

3. Internet connection because of some CDN Javascript libraries used in the test client.

---

## Run leaderboard service

1. Go to **LeaderboardService** folder.
2. Run command **npm install** then **node server.js**.

---

## Test leaderboard service by some webpages

The testing will be done in localhost.

1. Go to **LeaderboardClient** folder.
2. Run command **npm install** then **node app.js**.
3. Open *http://localhost:8000/* in more than 1 browser window/tab to test user stories.
4. Open *http://localhost:8000/admin* to test admin stories.

---
